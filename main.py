import sys, json, os

definition = json.load(sys.stdin)
tag = str(os.getenv('DOCKER_IMAGE_TAG')) if os.getenv('DOCKER_IMAGE_TAG') != None else 'latest'

containerDefinitions = definition['taskDefinition']['containerDefinitions']

for i in range(len(containerDefinitions)):
    containerDefinitions[i]['image'] = containerDefinitions[i]['image'].split(':')[0] + ':' + tag

registerTask = json.dumps({
    'volumes': definition['taskDefinition']['volumes'],
    'family': definition['taskDefinition']['family'],
    'executionRoleArn': definition['taskDefinition']['executionRoleArn'],
    'networkMode': definition['taskDefinition']['networkMode'],
    'containerDefinitions': containerDefinitions,
    'memory': definition['taskDefinition']['memory'],
    'cpu': definition['taskDefinition']['cpu']
})

print (registerTask)
